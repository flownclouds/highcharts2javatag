<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include.inc.jsp"%>
<div id="line"  class="main000"></div>
<highcharts:line
	id="line" 
	title="烟气图表汇总-除硫" 
	subtitle="烟气图表汇总-除硫" 
	x_categories_type="diy"
	yunitname="立方米(㎥)"
	xunitname="时间趋势"
	xlist="${xlist}"
	ylist="${ylist}"/>
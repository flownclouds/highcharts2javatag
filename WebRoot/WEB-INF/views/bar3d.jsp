<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/views/include.inc.jsp"%>

单轴(x轴自定义)：
<div id="bar3d-diy"  class="main000"></div>
<highcharts:bar3d 
	id="bar3d-diy" 
	title="烟气图表汇总-除硫" 
	subtitle="烟气图表汇总-除硫" 
	x_categories_type="diy"
	yunitname="立方米(㎥)"
	xunitname="时间趋势"
	xlist="${xlist}"
	ylist="${ylist}"/>
	
	
单轴(x轴系统选项：周)：
<div id="bar3d-weekdays"  class="main000"></div>
<highcharts:bar3d 
	id="bar3d-weekdays" 
	title="烟气图表汇总-除硫" 
	subtitle="烟气图表汇总-除硫" 
	x_categories_type="weekdays"
	yunitname="立方米(㎥)"
	xunitname="周趋势"
	xlist="${xlist}"
	ylist="${ylist}"/>
	
	
单轴(x轴系统选项：月)：
<div id="bar3d-months"  class="main000"></div>
<highcharts:bar3d 
	id="bar3d-months" 
	title="烟气图表汇总-除硫" 
	subtitle="烟气图表汇总-除硫" 
	x_categories_type="months"
	yunitname="立方米(㎥)"
	xunitname="月趋势"
	xlist="${xlist}"
	ylist="${ylist}"/>

	
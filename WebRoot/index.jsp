<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
  </head>
  
  <body>
	  <div>
		<a href="Bar3DDemo" target="_blank">3D条形(柱状)图Bar</a><br/>
		<img src="Bar3D.png" width="300px" height="200px"/>
		<img src="Bar3DTag.png" width="300px" height="200px"/><br/>
	  </div>
	   <div>
		<a href="Pie3DDemo" target="_blank">3D饼状图Pie</a><br/>
		<img src="Pie3D.png" width="300px" height="200px"/>
		<img src="Pie3DTag.png" width="300px" height="200px"/><br/>
	  </div>
	  <div>
		<a href="LineDemo" target="_blank">折线图line</a><br/>
		<img src="Line.png" width="300px" height="200px"/>
		<img src="LineTag.png" width="300px" height="200px"/><br/>
	  </div>
  </body>
</html>

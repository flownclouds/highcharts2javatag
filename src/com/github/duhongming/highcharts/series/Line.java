package com.github.duhongming.highcharts.series;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.data.DataLabels;
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Line {
	@Setter private DataLabels dataLabels;
	@Getter @Setter private boolean enableMouseTracking;
	public DataLabels dataLabels() {
		if (this.dataLabels == null) {
            this.dataLabels = new DataLabels();
        }
		return dataLabels;
	}
}

package com.github.duhongming.highcharts.series;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.data.DataLabels;
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Pie {
	@Getter @Setter private Boolean allowPointSelect;
	@Getter @Setter private String cursor;
	@Getter @Setter private Integer depth;
	@Setter private DataLabels dataLabels;
	
	
	public DataLabels dataLabels() {
		if (this.dataLabels == null) {
            this.dataLabels = new DataLabels();
        }
		return this.dataLabels;
	}

	
}

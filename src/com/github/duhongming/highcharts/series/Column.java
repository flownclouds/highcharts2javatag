package com.github.duhongming.highcharts.series;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.data.DataLabels;
/**
 * 
 * @author dhm
 * plotOptions.column
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Column {
	/**
	 * Depth of the columns in a 3D column chart. Requires highcharts-3d.js.
	 */
	@Getter @Setter private int depth;
	@Setter private DataLabels dataLabels;
	public DataLabels dataLabels() {
		if (this.dataLabels == null) {
            this.dataLabels = new DataLabels();
        }
		return dataLabels;
	}
}

package com.github.duhongming.highcharts.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * 
 * @author dhm
 * The Data module provides a simplified interface for adding data to a chart from sources like CVS, HTML tables or grid views.
 *  See also the tutorial article on the Data module.
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Data {
	
	/**
	 * The name of the point as shown in the legend, tooltip, dataLabel etc.
	 */
	@Getter @Setter private String name;
	
	/**
	 * The y value of the point.
	 */
	@Getter @Setter private Object y;
	
	/**
	 * Pie series only. Whether to display a slice offset from the center.
	 */
	@Getter @Setter private Boolean sliced;
	
	/**
	 * Whether to select the series initially. If showCheckbox is true, the checkbox next to the series name will be checked for a selected series.
	 */
	@Getter @Setter private Boolean selected; 
}

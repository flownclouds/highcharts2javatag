package com.github.duhongming.highcharts.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class DataLabels {
	
	@Getter @Setter private Boolean enabled;
	@Getter @Setter private String format;

}

package com.github.duhongming.highcharts.axis;

import com.github.duhongming.highcharts.Title;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * X轴或者类别轴通常是水平方向的坐标轴，但是通过 chart.inverted = true 
 * 可以让x，y轴显示位置对调此时垂直的坐标轴是X轴。如果有多条坐标轴，x轴节点是一个可配置的对象数组。
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class XAxis {
	/**
	 * 轴标题的显示文本。它可以包含类似的，基本的HTML标签。
	 * 但是文本的旋转使用向量绘制技术实现,有些文本样式会清除。通过设置text为null来禁用轴标题
	 */
	private Title title;
	/**
	 * 类别名称
	 */
	@Getter @Setter private Object[] categories;	
	
	public Title title() {
		if (this.title == null) {
            this.title = new Title();
        }
		return title;
	}
}

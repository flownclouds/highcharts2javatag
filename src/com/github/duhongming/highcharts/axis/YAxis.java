package com.github.duhongming.highcharts.axis;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.Title;
import com.github.duhongming.highcharts.d3.PlotLines;
/**
 * Y轴或者值轴。通常情况下，y轴是垂直的，通过 chart.inverted = true 
 * 可以让x，y轴显示位置对调，即水平的坐标轴是Y轴。多轴情况下，yAxis 是一个y轴配置数组。
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class YAxis {
	
	/**
	 * 轴标题的显示文本。它可以包含类似的，基本的HTML标签。
	 * 但是文本的旋转使用向量绘制技术实现,有些文本样式会清除。通过设置text为null来禁用轴标题
	 */
	private Title title;
	
	@Getter @Setter private PlotLines[] plotLines;

	public Title title() {
		if (this.title == null) {
            this.title = new Title();
        }
		return title;
	}
	
	public YAxis title(String text) {
		this.title().text(text);
		return this;
	}
	
	
}

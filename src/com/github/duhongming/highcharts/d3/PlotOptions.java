package com.github.duhongming.highcharts.d3;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.series.Bar;
import com.github.duhongming.highcharts.series.Column;
import com.github.duhongming.highcharts.series.Line;
import com.github.duhongming.highcharts.series.Pie;
/**
 * The plotOptions is a wrapper object for config objects for each series type. 
 * The config objects for each series can also be overridden for each series item as given in the series array.
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class PlotOptions {
	@Setter private Line line;
	@Setter private Bar bar;
	@Setter private Pie pie;
	
	@Setter private Column column;
	
	public Pie pie() {
		if (this.pie == null) {
            this.pie = new Pie();
        }
		return pie;
	}
	
	public Line line() {
		if (this.line == null) {
            this.line = new Line();
        }
		return line;
	}
	
	public Bar bar() {
		if (this.bar == null) {
            this.bar = new Bar();
        }
		return bar;
	}

	public Column column() {
		if (this.column == null) {
            this.column = new Column();
        }
		return column;
	}	
	
}

package com.github.duhongming.highcharts.d3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 通过颜色线横贯在绘图区域上标记轴中的一个特定值
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class PlotLines {
	/**
	 * 区域划分线代表的值
	 */
	@Getter @Setter private Double value;
	
	/**
	 * 区域划分线的宽度或者厚度
	 */
	@Getter @Setter private Double width;
	
	/**
	 * 区域划分线的颜色
	 */
	@Getter @Setter private String color;
	
}

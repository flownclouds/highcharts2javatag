package com.github.duhongming.highcharts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * 图表副标题
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Subtitle {
	//图表的副标题文字，默认是空，即默认是没有副标题的。
	
	/**
	 * 图表标题水平对齐方式。有“"left", "center" and "right"可选，分别对应“左对齐”、“居中对齐”、“右对齐”。
	 * 默认是： center.
	 */
	@Getter @Setter private String align;
	
	/**
	 * 标题是否浮动。当设置浮动（即该属性值为true）时，标题将不占空间。
	 */
	@Getter @Setter private Boolean floating;
	
	/**
	 * 标题是否使用HTML渲染，是否解析html标签，设置解析后，可以使用例如 a 等 html 标签。
	 */
	@Getter @Setter private Boolean useHTML;
	
	/**
	 * 图表的标题名，如果想不显示标题，把text设置为null。
	 */
	@Getter @Setter private String text;
}

package com.github.duhongming.highcharts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.axis.XAxis;
import com.github.duhongming.highcharts.axis.YAxis;
import com.github.duhongming.highcharts.d3.PlotOptions;
import com.github.duhongming.highcharts.series.Series;
@Accessors(fluent = true)
@SuppressWarnings("rawtypes")
public class Option {
	
	/**
	 * 关于图表区和图形区的参数及一般图表通用参数
	 */
	
	@Setter private Chart chart;
	
	 /**
     * 图表标题
     */
	@NonNull private Title title;
	
	/**
	 * 图表副标题
	 */
	private Subtitle subtitle;
	
	/**
	 * X轴或者类别轴通常是水平方向的坐标轴，但是通过 chart.inverted = true 
	 * 可以让x，y轴显示位置对调此时垂直的坐标轴是X轴。如果有多条坐标轴，x轴节点是一个可配置的对象数组。
	 */
	@Setter private XAxis xAxis;
	
	/**
	 * Y轴或者值轴。通常情况下，y轴是垂直的，通过 chart.inverted = true 
	 * 可以让x，y轴显示位置对调，即水平的坐标轴是Y轴。多轴情况下，yAxis 是一个y轴配置数组。
	 */
	@Setter private YAxis yAxis;
	
	/**
	 * 数据提示框。当鼠标悬停在某点上时，以框的形式提示该点的数据，
	 * 比如该点的值，数据单位等。数据提示框内提示的信息完全可以通过格式化函数动态指定。
	 */
	@Setter private Tooltip tooltip;
	
	 /**
     * 驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数据
     * The actual series to append to the chart. In addition to the members listed below, any member of the plotOptions for that 
     * specific type of plot can be added to a series individually. For example, even though a general lineWidth is specified in plotOptions.
     * series, an individual lineWidth can be specified for each series.
     */
	private List<Series> series;
	
	/**
	 * The plotOptions is a wrapper object for config objects for each series type. 
	 * The config objects for each series can also be overridden for each series item as given in the series array.
	 */
	@Setter private PlotOptions plotOptions;
	
	public Chart chart() {
		if (this.chart == null) {
            this.chart = new Chart();
        }
        return this.chart;
	}
	
    public Title title() {
        if (this.title == null) {
            this.title = new Title();
        }
        return this.title;
    }
    public Subtitle subtitle() {
        if (this.subtitle == null) {
            this.subtitle = new Subtitle();
        }
        return this.subtitle;
    }
    
    /**
     * 标题和副标题
     *
     * @param text
     * @param subtext
     * @return
     */
    public Option title(String text, String subtext) {
        this.title().text(text);
        this.subtitle().text(subtext);
        return this;
    }
    
	public XAxis xAxis() {
		if (this.xAxis == null) {
            this.xAxis = new XAxis();
        }
		return this.xAxis;
	}
	
	public YAxis yAxis() {
		if (this.yAxis == null) {
            this.yAxis = new YAxis();
        }
		return this.yAxis;
	}
	
	public Tooltip tooltip() {
		if (this.tooltip == null) {
            this.tooltip = new Tooltip();
        }
		return this.tooltip;
	}

	/**
     * 驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数据
     */
    public List<Series> series() {
        if (this.series == null) {
            this.series = new ArrayList<Series>();
        }
        return this.series;
    }
    
	/**
     * 添加数据
     *
     * @param values
     * @return
     */
    public Option series(Series... values) {
        if (values == null || values.length == 0) {
            return this;
        }
        this.series().addAll(Arrays.asList(values));
        return this;
    }

	public PlotOptions plotOptions() {
		if (this.plotOptions == null) {
            this.plotOptions = new PlotOptions();
        }
		return this.plotOptions;
	}
}

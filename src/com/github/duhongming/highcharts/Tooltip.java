package com.github.duhongming.highcharts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * 数据提示框。当鼠标悬停在某点上时，以框的形式提示该点的数据，比如该点的值，数据单位等。
 * 数据提示框内提示的信息完全可以通过格式化函数动态指定。
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Tooltip {
	/**
	 * 一串字符被前置在每个Y轴的值之前。可重写在每个系列的提示框选项的对象上。
	 */
	@Getter @Setter private String valueSuffix;
	/**
	 * A callback function for formatting the HTML output for a single point in the tooltip. 
	 * Like the pointFormat string, but with more flexibility.
	 */
	@Getter @Setter private String pointFormat;
}

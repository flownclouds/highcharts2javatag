package com.github.duhongming.highcharts;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.github.duhongming.highcharts.d3.Options3d;
import com.github.duhongming.highcharts.series.SeriesType;
/**
 * 关于图表区和图形区的参数及一般图表通用参数。
 * @author dhm
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class Chart<T> {
	/**
	 * 图表描绘出后放到页面的某一具体位置
	 */
	@Getter @Setter private String renderTo;
	
	/**
	 * 指定绘制区所要绘制的图的类型，例如：type=bar为柱图，type=line为线图
	 */
	private SeriesType type;
	
	@Getter @Setter private Boolean inverted;
	
	/**
	 * 3D图像设置项。3D效果需要引入highcharts-3d.js，下载或者在线路径为code.highcharts.com/highcharts-3d.js.
	 */
	@Setter private Options3d options3d;
	
	/**
	 * 边距是指图表的外边与图形区域之间的距离，数组分别代表上、右、下和左。要想单独设置可以用marginTop,marginRight,marginBotton 和 marginLeft.
	 */
	@Getter @Setter private int[] margin;
	
	public Options3d options3d() {
		 if (this.options3d == null) {
            this.options3d = new Options3d();
         }
	     return this.options3d;
	}	
	/**
     * 获取type值
     */
    public SeriesType type() {
        return this.type;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    @SuppressWarnings("unchecked")
	public T type(SeriesType type) {
        this.type = type;
        return (T) this;
    }
}

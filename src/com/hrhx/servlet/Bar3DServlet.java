package com.hrhx.servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name = "Bar3DServlet", urlPatterns = { "/Bar3DDemo" }, loadOnStartup = 1)
public class Bar3DServlet extends HttpServlet {
	private static final long serialVersionUID = -6886697421555222670L;
	
	private List<String> xlist;
	
	private Map<String,List<Double>> ylist;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		this.doPost(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//x轴数据
		request.setAttribute("xlist", getXList());
		//y轴数据
		request.setAttribute("ylist", getYList());

		request.getRequestDispatcher("WEB-INF/views/bar3d.jsp").forward(request, response);
	}
	
	public List<String> getXList(){
		xlist = new ArrayList<String>();
		xlist.add("2015-10-10");
		xlist.add("2015-10-11");
		xlist.add("2015-10-12");
		xlist.add("2015-10-13");
		xlist.add("2015-10-14");
		return xlist;
	}
	
	public Map<String,List<Double>> getYList(){
		Random random = new Random();
		ylist = new HashMap<String,List<Double>>();
		
		List<Double> data1 = new ArrayList<Double>();
		data1.add(random.nextDouble());
		data1.add(random.nextDouble());
		data1.add(random.nextDouble());
		data1.add(random.nextDouble());
		data1.add(random.nextDouble());
		ylist.put("柱状一", data1);
		
		List<Double> data2 = new ArrayList<Double>();
		data2.add(random.nextDouble());
		data2.add(random.nextDouble());
		data2.add(random.nextDouble());
		data2.add(random.nextDouble());
		data2.add(random.nextDouble());
		ylist.put("柱状二", data2);
		
		return ylist;
	}

	
	

}

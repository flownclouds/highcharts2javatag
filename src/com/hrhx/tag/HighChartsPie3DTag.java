package com.hrhx.tag;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

import com.github.duhongming.highcharts.data.Data;
import com.github.duhongming.highcharts.json.GsonOption;
import com.github.duhongming.highcharts.series.Series;
import com.github.duhongming.highcharts.series.SeriesType;

public class HighChartsPie3DTag extends BodyTagSupport{
	private static final long serialVersionUID = 1L;
	private String title;
	private String subtitle;
	private String id;
	private Map<String,Double> legendMap;
	private String selected;
	@Override
	public int doStartTag() throws JspException {
		return BodyTag.EVAL_BODY_BUFFERED;
	
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int doEndTag() throws JspException {
		//js开始位置
		String jsFuncStart="<script type=text/javascript>$(function () {$('#"+id+"').highcharts(";
		//js结束位置
		String jsFuncEnd=");});</script>";
		
		//BodyContent bc= this.getBodyContent();
		//String content =bc.getString();
		GsonOption option = new GsonOption();
		//设置标题、设置子标题
		option.title(title, subtitle);
		//设置3D状态
		option.chart().options3d().enabled(true).alpha(45).beta(0);
		option.chart().type(SeriesType.pie);
		//工具显示框
		option.tooltip().pointFormat("{series.name}: <b>{point.percentage:.1f}%</b>");
		//3D柱状图配置
		option.plotOptions().pie().allowPointSelect(true)
								  .cursor("pointer")
								  .depth(35)
								  .dataLabels().enabled(true).format("<b>{point.name}</b>: {point.percentage:.2f} %");
		
		if(legendMap!=null){
			Set<Entry<String, Double>>  set = legendMap.entrySet();
			Object[] obj= new Object[set.size()];
			int i=0;
			for (String key :legendMap.keySet()) {
				Object yvalue=legendMap.get(key);
				Data data=new Data(key,yvalue,false,false);
				if(selected!=null&&selected.equals(key)){
					data.selected(true);
					data.sliced(true);
				}
				obj[i]=data;
				i++;
			}
			Series series = new Series(subtitle,SeriesType.pie,obj);
			option.series(series);
		}
		
		try {
			this.pageContext.getOut().write(jsFuncStart+option.toString()+jsFuncEnd);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return Tag.EVAL_PAGE;//继续处理页面
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle){
		this.subtitle = subtitle;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public Map<String, Double> getLegendMap() {
		return legendMap;
	}

	public void setLegendMap(Map<String, Double> legendMap) {
		this.legendMap = legendMap;
	}

}

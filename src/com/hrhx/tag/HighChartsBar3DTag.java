package com.hrhx.tag;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

import com.github.duhongming.highcharts.json.GsonOption;
import com.github.duhongming.highcharts.series.Series;
import com.github.duhongming.highcharts.series.SeriesType;

public class HighChartsBar3DTag extends BodyTagSupport{
	private static final long serialVersionUID = 1L;
	private String title;
	private String subtitle;
	private String id;
	private String x_categories_type;
	private String xunitname;
	private String yunitname;
	private List<String> xlist;
	private Map<String,List<Object>> ylist;
	@Override
	/**
		EVAL_BODY_INCLUDE：把Body读入存在的输出流中，doStartTag()函数可用
		EVAL_PAGE：继续处理页面，doEndTag()函数可用
		SKIP_BODY：忽略对Body的处理，doStartTag()和doAfterBody()函数可用
		SKIP_PAGE：忽略对余下页面的处理，doEndTag()函数可用
		EVAL_BODY_TAG：已经废止，由EVAL_BODY_BUFFERED取代
		EVAL_BODY_BUFFERED：申请缓冲区，由setBodyContent()函数得到的BodyContent对象来处理tag的body，
		如果类实现了BodyTag，那么doStartTag()可用，否则非法
	 */
	public int doStartTag() throws JspException {
		return BodyTag.EVAL_BODY_BUFFERED;//申请缓冲区
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int doEndTag() throws JspException {
		//js开始位置
		String jsFuncStart="<script type=text/javascript>$(function () {$('#"+id+"').highcharts(";
		//js结束位置
		String jsFuncEnd=");});</script>";
		//得到标签体里面的内容
		//BodyContent bc= this.getBodyContent();
		//String content =bc.getString();
		GsonOption option = new GsonOption();
		/**
		 * title: {
            	text: '3D chart with null values'
	        },
	        subtitle: {
	            text: 'Notice the difference between a 0 value and a null point'
	        },
		 */
		//设置标题、设置子标题
		option.title(title,subtitle);
		/**
		 * chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 10,
                beta: 25,
                depth: 70
            }
         },
		 */
		//设置3D状态
		option.chart().margin(new int[]{75,75,95,75}).inverted(false).type(SeriesType.column);
		option.chart().options3d().enabled(true).alpha(10).beta(25).depth(70);
		/**
		 *  plotOptions: {
	            column: {
	                depth: 25
	            }
        	},
		 */
		option.plotOptions().column().depth(25)
									 .dataLabels().enabled(true).format("{point.y:.2f}%");
		/**
		 * xAxis: {
            categories: Highcharts.getOptions().lang.shortMonths
        	},
		 */
		//设置x轴
		String[] categories_month={"一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"};
		String[] categories_week={"星期一", "星期二", "星期三", "星期三", "星期四", "星期五", "星期六","星期天"};
		option.xAxis().title().text(xunitname);
		if(x_categories_type.equalsIgnoreCase("months")){
			option.xAxis().categories(categories_month);
		}else if(x_categories_type.equalsIgnoreCase("weekdays")){
			option.xAxis().categories(categories_week);
		}else{
			option.xAxis().categories(xlist.toArray());
		}
		/**
		 * yAxis: {
            title: {
                text: "11"
            }
        },
		 */
		//设置y轴
		option.yAxis().title(yunitname);
		
		
		//顶层map的记录数
		Set<Entry<String, List<Object>>>  set = ylist.entrySet();
		Series[] series =new  Series[set.size()];
		//遍历顶层map，并得到list
		int i=0;
		for (String key :ylist.keySet()) {
			//遍历list得到数据
			List<Object> list=ylist.get(key);
			Double [] data= new Double [list.size()];
			int j=0;
			for(Object obj :list){
				if(obj!=null){
					data[j]=Double.parseDouble(obj.toString());
				}else{
					data[j]=null;
				}
				j++;
			}
			series[i] = new Series().data(data).name(key);
			i++;
		}
		option.series(series);
		try {
			this.pageContext.getOut().write(jsFuncStart+option.toString()+jsFuncEnd);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Tag.EVAL_PAGE;//继续处理页面
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle){
		this.subtitle = subtitle;
	}

	public String getX_categories_type() {
		return x_categories_type;
	}

	public void setX_categories_type(String x_categories_type) {
		this.x_categories_type = x_categories_type;
	}

	public List<String> getXlist() {
		return xlist;
	}

	public void setXlist(List<String> xlist) {
		this.xlist = xlist;
	}

	public Map<String, List<Object>> getYlist() {
		return ylist;
	}

	public void setYlist(Map<String, List<Object>> ylist) {
		this.ylist = ylist;
	}

	public String getXunitname() {
		return xunitname;
	}

	public void setXunitname(String xunitname) {
		this.xunitname = xunitname;
	}

	public String getYunitname() {
		return yunitname;
	}

	public void setYunitname(String yunitname) {
		this.yunitname = yunitname;
	}



}

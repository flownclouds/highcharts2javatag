# highcharts2javatag
通过自定义标签实现highcharts 3D图表展示，借鉴了Android适配器的思想，通过固定的数据源，展现相关图表，无需考虑图表内部实现。

**演示地址:**
http://dtmonitor.tunnel.qydev.com/highcharts2javatag/

官网地址：
https://projectlombok.org/

官网使用说明：
http://jnb.ociweb.com/jnb/jnbJan2010.html

java -jar lombok.jar

**Maven:**
```xml
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>[2.6.2,)</version>
</dependency>
```

**3D条形(柱状)图Bar的Tag:**
```xml
	<highcharts:bar3d 
		id="bar3d" 
		title="烟气图表汇总-除硫" 
		subtitle="烟气图表汇总-除硫" 
		x_categories_type="diy"
		yunitname="立方米(㎥)"
		xlist="${xlist}"
		ylist="${ylist}"/>
```

**3D条形(柱状)图Bar的数据格式:**		

	private List<String> xlist;
	
	private Map<String,List<Double>> ylist;
	
	
**3D饼状图Pie的Tag:**
```xml
	<highcharts:pie3d
		id="pie" 
		title="烟气图表汇总-除硫" 
		subtitle="烟气图表汇总-除硫" 
		legendMap="${legendMap}"/>
```

**3D饼状图Pie的数据格式:**		

	private Map<String,Object> legendMap;
	
**折线图line的Tag:**
```xml
	<highcharts:bar3d 
		id="bar3d" 
		title="烟气图表汇总-除硫" 
		subtitle="烟气图表汇总-除硫" 
		x_categories_type="diy"
		yunitname="立方米(㎥)"
		xlist="${xlist}"
		ylist="${ylist}"/>
```

**折线图line的数据格式:**		

	private List<String> xlist;
	
	private Map<String,List<Double>> ylist;